﻿using dev.christensen.imagepicker.model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace dev.christensen.imagepicker.infrastructure
{
    public class ImagePickerContext : DbContext
    {
        public ImagePickerContext(DbContextOptions<ImagePickerContext> options) : base(options)
        {

        }

        public DbSet<Image> Images { get; set; }
        public DbSet<ImageCollection> ImageCollections { get; set; }
        public DbSet<Collection> Collections { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }

    }
}
