using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using dev.christensen.imagepicker.infrastructure;
using dev.christensen.imagepicker.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dev.christensen.imagepicker.web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CollectionController : ControllerBase
  {
    ImagePickerContext context { get; set; }
    public CollectionController(ImagePickerContext context)
    {
      this.context = context;
    }

    [HttpGet("{Id}")]
    public Collection GetCollection(Guid Id)
    {
      if (context.Collections.Any(c => c.Id == Id))
        return context.Collections.First(c => c.Id == Id);
      return null;

    }

    [HttpGet]
    public IEnumerable<Collection> List()
    {
      return context.Collections.ToList();
    }

    [HttpPost]
    public Collection Create([FromBody] Collection collection)
    {
      collection.Id = Guid.NewGuid();
      context.Add(collection);
      context.SaveChanges();
      return collection;
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(Guid Id)
    {
      if(context.Collections.Any(c => c.Id == Id))
      {
        var c = context.Collections.First(cc => cc.Id == Id);
        context.Remove(c);
        context.SaveChanges();
        return NoContent();
      }
      return new StatusCodeResult(StatusCodes.Status304NotModified);
      
    }

  }
}
