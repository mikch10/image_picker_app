using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using dev.christensen.imagepicker.infrastructure;
using dev.christensen.imagepicker.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace dev.christensen.imagepicker.web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ImageController : ControllerBase
  {

    ImagePickerContext context;
    public ImageController(ImagePickerContext context)
    {
      this.context = context;
    }

    [HttpPost, DisableRequestSizeLimit]
    [Route("upload")]
    public async Task<string> Upload()
    {
      try
      {
        var file = Request.Form.Files[0];
        Image img = new Image();
        if (file.Length > 0)
        {
          string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
          using (var stream = new MemoryStream())
          {
            file.OpenReadStream().CopyTo(stream);
            img.ImageData = stream.ToArray();
            img.ImageTitle = file.FileName;
          }
          context.Add(img);
          context.SaveChanges();
          await context.SaveChangesAsync();
        }

      }
      catch (Exception e)
      {
        throw e;
      }
      return JsonConvert.SerializeObject("endpoint works");
    }

    [HttpGet]
    public IEnumerable<Guid> GetAllImages()
    {
      return context.Images.Select(m => m.Id).ToList();
    }

    [HttpGet("count")]
    public int GetImageCount()
    {
      if (context.Images.Count() > 0)
      {
        return context.Images.Count();
      }
      return 0;
    }

    [HttpGet]
    //[Authorize]
    [Route("{Id}")]
    public FileResult GetByNumber(Guid Id)
    {
      var image = context.Images.Where(q => q.Id == Id).FirstOrDefault();
      if (image != null)
      {
        return File(image.ImageData, "image/jpeg");
      }
      return null;
    }

  }
}
