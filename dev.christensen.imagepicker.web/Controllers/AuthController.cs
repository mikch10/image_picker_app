using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using dev.christensen.imagepicker.model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dev.christensen.imagepicker.web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AuthController : ControllerBase
  {

    [HttpGet("login")]
    public async Task<User> Authorize(string username = "", string password = "")
    {
      // pls no steal my creds mr. code reviewer.
      SHA256 sha = SHA256.Create();
      var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
      var hashedPW = string.Concat(hash.Select(b => b.ToString("x2")));

      if (username == "farogtrinne" && hashedPW == "9c9a6ec2286bca09144f1a104250a6e78d95ba132de29754671dd232f4130ae4")
      {
        var ci = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(ci), new AuthenticationProperties { IsPersistent = false });
        return new User
        {
          Username = "Far, og Trinne"
        };
      }
      return null;
    }

    [Authorize]
    public string Test()
    {
      var m = HttpContext.User;
      return "success";
    }

  }
}
