(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_upload_upload_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/upload/upload.component */ "./src/app/components/upload/upload.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_collection_collection_main_collection_main_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/collection/collection-main/collection-main.component */ "./src/app/components/collection/collection-main/collection-main.component.ts");
/* harmony import */ var _components_picture_browser_browser_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/picture/browser/browser.component */ "./src/app/components/picture/browser/browser.component.ts");









var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
    { path: 'collection', component: _components_collection_collection_main_collection_main_component__WEBPACK_IMPORTED_MODULE_7__["CollectionMainComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: 'browse', component: _components_picture_browser_browser_component__WEBPACK_IMPORTED_MODULE_8__["BrowserComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: 'upload', component: _components_upload_upload_component__WEBPACK_IMPORTED_MODULE_3__["UploadComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"grid\">\n    <div class=\"grid-top\">\n        <app-navigation></app-navigation>\n    </div>\n    <div class=\"grid-content\">\n        <router-outlet id=\"test\"></router-outlet>\n    </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/app.component.less":
/*!************************************!*\
  !*** ./src/app/app.component.less ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQubGVzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'imgapp';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.less */ "./src/app/app.component.less")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_upload_upload_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/upload/upload.component */ "./src/app/components/upload/upload.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/navigation/navigation.component */ "./src/app/components/navigation/navigation.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_collection_collection_main_collection_main_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/collection/collection-main/collection-main.component */ "./src/app/components/collection/collection-main/collection-main.component.ts");
/* harmony import */ var _components_picture_browser_browser_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/picture/browser/browser.component */ "./src/app/components/picture/browser/browser.component.ts");
/* harmony import */ var _components_home_collection_card_collection_card_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/home/collection-card/collection-card.component */ "./src/app/components/home/collection-card/collection-card.component.ts");
/* harmony import */ var _components_home_card_browser_card_browser_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/home/card-browser/card-browser.component */ "./src/app/components/home/card-browser/card-browser.component.ts");
/* harmony import */ var _components_collection_collection_item_collection_item_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/collection/collection-item/collection-item.component */ "./src/app/components/collection/collection-item/collection-item.component.ts");
/* harmony import */ var _components_collection_collection_list_collection_list_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/collection/collection-list/collection-list.component */ "./src/app/components/collection/collection-list/collection-list.component.ts");

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_upload_upload_component__WEBPACK_IMPORTED_MODULE_5__["UploadComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_9__["NavigationComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
                _components_collection_collection_main_collection_main_component__WEBPACK_IMPORTED_MODULE_11__["CollectionMainComponent"],
                _components_picture_browser_browser_component__WEBPACK_IMPORTED_MODULE_12__["BrowserComponent"],
                _components_home_collection_card_collection_card_component__WEBPACK_IMPORTED_MODULE_13__["CollectionCardComponent"],
                _components_home_card_browser_card_browser_component__WEBPACK_IMPORTED_MODULE_14__["CardBrowserComponent"],
                _components_collection_collection_item_collection_item_component__WEBPACK_IMPORTED_MODULE_15__["CollectionItemComponent"],
                _components_collection_collection_list_collection_list_component__WEBPACK_IMPORTED_MODULE_16__["CollectionListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        return this.checkLogin();
    };
    AuthGuard.prototype.checkLogin = function () {
        return true;
        if (this.authService.isLoggedIn) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/components/collection/collection-item/collection-item.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-item/collection-item.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"collection-item\">\n  <div class=\"name\">\n      {{collection.collectionName}}\n  </div>\n  <div class=\"detail\">\n      <button (click)='delete()'>Remove</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/collection/collection-item/collection-item.component.less":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-item/collection-item.component.less ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  src: url('IndieFlower.ttf');\n  font-family: flower;\n}\n@font-face {\n  src: url('Raleway-Regular.ttf');\n  font-family: raleway;\n}\n.collection-item {\n  width: 100%;\n  height: 3em;\n  background-color: #b488b0;\n  padding: 0.5em;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  margin: 0.125em;\n  color: #fafafa;\n  font-weight: bold;\n}\n.collection-item > * {\n  flex-grow: 1;\n  max-width: 50%;\n}\n.detail {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n.detail > button {\n  background-color: #DA627D;\n  font-weight: bold;\n  padding: 0.5em;\n  border: none;\n  min-width: 120px;\n  color: #fafafa;\n}\n.detail > button:hover {\n  background-color: #e694a6;\n  cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24taXRlbS9FOi9EZXZlbG9wL0ltYWdlUGlja2VyL2Rldi5jaHJpc3RlbnNlbi5pbWFnZXBpY2tlci53ZWIvc3JjL2Fzc2V0cy92YXJpYWJsZXMubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24taXRlbS9jb2xsZWN0aW9uLWl0ZW0uY29tcG9uZW50Lmxlc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29sbGVjdGlvbi9jb2xsZWN0aW9uLWl0ZW0vRTovRGV2ZWxvcC9JbWFnZVBpY2tlci9kZXYuY2hyaXN0ZW5zZW4uaW1hZ2VwaWNrZXIud2ViL3NyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24taXRlbS9jb2xsZWN0aW9uLWl0ZW0uY29tcG9uZW50Lmxlc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0E7RUFDSSwyQkFBQTtFQUNBLG1CQUFBO0FDSko7QURPQTtFQUNJLCtCQUFBO0VBQ0Esb0JBQUE7QUNMSjtBQ0xBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QURPSjtBQ05JO0VBQ0ksWUFBQTtFQUNBLGNBQUE7QURRUjtBQ0pBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QURNSjtBQ0xDO0VBQ0cseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FET0o7QUNOSTtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtBRFFSIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24taXRlbS9jb2xsZWN0aW9uLWl0ZW0uY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY29sb3ItYmFja2dyb3VuZDogIzgzNTM3RjtcbkBjb2xvci1iYWNrZ3JvdW5kLXNlY29uZGFyeTogI2YzZjNmZjtcbkBjb2xvci1hY2NlbnQ6ICNEQTYyN0Q7XG5AcGFnZS13aWR0aDogMTAyNHB4O1xuXG5AZm9udC1mYWNlIHtcbiAgICBzcmM6IHVybChcIkluZGllRmxvd2VyLnR0ZlwiKTtcbiAgICBmb250LWZhbWlseTogZmxvd2VyO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgICBzcmM6IHVybChcIlJhbGV3YXktUmVndWxhci50dGZcIik7XG4gICAgZm9udC1mYW1pbHk6IHJhbGV3YXk7XG59IiwiQGZvbnQtZmFjZSB7XG4gIHNyYzogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL0luZGllRmxvd2VyLnR0ZlwiKTtcbiAgZm9udC1mYW1pbHk6IGZsb3dlcjtcbn1cbkBmb250LWZhY2Uge1xuICBzcmM6IHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9SYWxld2F5LVJlZ3VsYXIudHRmXCIpO1xuICBmb250LWZhbWlseTogcmFsZXdheTtcbn1cbi5jb2xsZWN0aW9uLWl0ZW0ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICNiNDg4YjA7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbjogMC4xMjVlbTtcbiAgY29sb3I6ICNmYWZhZmE7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNvbGxlY3Rpb24taXRlbSA+ICoge1xuICBmbGV4LWdyb3c6IDE7XG4gIG1heC13aWR0aDogNTAlO1xufVxuLmRldGFpbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4uZGV0YWlsID4gYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RBNjI3RDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICBib3JkZXI6IG5vbmU7XG4gIG1pbi13aWR0aDogMTIwcHg7XG4gIGNvbG9yOiAjZmFmYWZhO1xufVxuLmRldGFpbCA+IGJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjk0YTY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbiIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uL2Fzc2V0cy92YXJpYWJsZXMubGVzcyc7XG5cbi5jb2xsZWN0aW9uLWl0ZW0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogM2VtO1xuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0ZW4oQGNvbG9yLWJhY2tncm91bmQsIDIwKTtcbiAgICBwYWRkaW5nOiAuNWVtO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgbWFyZ2luOiAuMTI1ZW07XG4gICAgY29sb3I6ICNmYWZhZmE7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgJiA+ICoge1xuICAgICAgICBmbGV4LWdyb3c6IDE7XG4gICAgICAgIG1heC13aWR0aDogNTAlO1xuICAgIH1cbn1cblxuLmRldGFpbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gJiA+IGJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0RBNjI3RDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nOiAwLjVlbTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWluLXdpZHRoOiAxMjBweDtcbiAgICBjb2xvcjogI2ZhZmFmYTtcbiAgICAmOmhvdmVyIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2OTRhNjtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/components/collection/collection-item/collection-item.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/collection/collection-item/collection-item.component.ts ***!
  \************************************************************************************/
/*! exports provided: CollectionItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionItemComponent", function() { return CollectionItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_collection_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../model/collection.model */ "./src/app/model/collection.model.ts");
/* harmony import */ var src_app_services_collection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/collection.service */ "./src/app/services/collection.service.ts");




var CollectionItemComponent = /** @class */ (function () {
    function CollectionItemComponent(collectionService) {
        this.collectionService = collectionService;
    }
    CollectionItemComponent.prototype.ngOnInit = function () {
    };
    CollectionItemComponent.prototype.delete = function () {
        var _this = this;
        this.collectionService.delete(this.collection.id).subscribe(function (r) { return _this.collectionService.list(); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _model_collection_model__WEBPACK_IMPORTED_MODULE_2__["Collection"])
    ], CollectionItemComponent.prototype, "collection", void 0);
    CollectionItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-collection-item',
            template: __webpack_require__(/*! ./collection-item.component.html */ "./src/app/components/collection/collection-item/collection-item.component.html"),
            styles: [__webpack_require__(/*! ./collection-item.component.less */ "./src/app/components/collection/collection-item/collection-item.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_collection_service__WEBPACK_IMPORTED_MODULE_3__["CollectionService"]])
    ], CollectionItemComponent);
    return CollectionItemComponent;
}());



/***/ }),

/***/ "./src/app/components/collection/collection-list/collection-list.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-list/collection-list.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-collection-item *ngFor='let c of collections' [collection]='c'></app-collection-item>"

/***/ }),

/***/ "./src/app/components/collection/collection-list/collection-list.component.less":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-list/collection-list.component.less ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29sbGVjdGlvbi9jb2xsZWN0aW9uLWxpc3QvY29sbGVjdGlvbi1saXN0LmNvbXBvbmVudC5sZXNzIn0= */"

/***/ }),

/***/ "./src/app/components/collection/collection-list/collection-list.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/collection/collection-list/collection-list.component.ts ***!
  \************************************************************************************/
/*! exports provided: CollectionListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionListComponent", function() { return CollectionListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CollectionListComponent = /** @class */ (function () {
    function CollectionListComponent() {
    }
    CollectionListComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], CollectionListComponent.prototype, "collections", void 0);
    CollectionListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-collection-list',
            template: __webpack_require__(/*! ./collection-list.component.html */ "./src/app/components/collection/collection-list/collection-list.component.html"),
            styles: [__webpack_require__(/*! ./collection-list.component.less */ "./src/app/components/collection/collection-list/collection-list.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CollectionListComponent);
    return CollectionListComponent;
}());



/***/ }),

/***/ "./src/app/components/collection/collection-main/collection-main.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-main/collection-main.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Opret en ny kollektion</h1>\n<form>\n  <div>\n    <label for=\"col_name\">Navngiv Din Kollektion:</label>\n    <input type=\"text\" id=\"col_name\" #colname />\n  </div>\n  <div>\n    <button (click)=\"create(colname)\">Opret</button>\n  </div>\n</form>\n\n<h2 class=\"mt-3\">Kollektioner</h2>\n<app-collection-list [collections]='collections'></app-collection-list>"

/***/ }),

/***/ "./src/app/components/collection/collection-main/collection-main.component.less":
/*!**************************************************************************************!*\
  !*** ./src/app/components/collection/collection-main/collection-main.component.less ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  src: url('IndieFlower.ttf');\n  font-family: flower;\n}\n@font-face {\n  src: url('Raleway-Regular.ttf');\n  font-family: raleway;\n}\nform {\n  margin: 0 auto;\n  width: 100%;\n  background-color: #83537F;\n  color: #fafafa;\n  padding: 4em;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\nform > div {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 0.5em;\n}\nform > div > label {\n  padding-right: 0.5em;\n  margin-right: 0.5em;\n}\nform > div > input {\n  padding: 0.5em 0;\n  margin: -0.5em;\n  border: none;\n  border-bottom: 2px solid #dbdbdb;\n  background: none;\n  box-shadow: none;\n  color: #fafafa;\n  width: 220px;\n  transition: 0.1s ease-in-out all;\n}\nform > div > input:focus {\n  outline: none;\n  border-bottom: 2px solid #e694a6;\n  color: #e694a6;\n}\nform > div > button {\n  background-color: #DA627D;\n  font-weight: bold;\n  padding: 0.5em;\n  border: none;\n  min-width: 120px;\n  color: #fafafa;\n  margin-top: 2em;\n  margin-bottom: -2em;\n}\nform > div > button:hover {\n  background-color: #e694a6;\n  cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24tbWFpbi9FOi9EZXZlbG9wL0ltYWdlUGlja2VyL2Rldi5jaHJpc3RlbnNlbi5pbWFnZXBpY2tlci53ZWIvc3JjL2Fzc2V0cy92YXJpYWJsZXMubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24tbWFpbi9jb2xsZWN0aW9uLW1haW4uY29tcG9uZW50Lmxlc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29sbGVjdGlvbi9jb2xsZWN0aW9uLW1haW4vRTovRGV2ZWxvcC9JbWFnZVBpY2tlci9kZXYuY2hyaXN0ZW5zZW4uaW1hZ2VwaWNrZXIud2ViL3NyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24tbWFpbi9jb2xsZWN0aW9uLW1haW4uY29tcG9uZW50Lmxlc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0E7RUFDSSwyQkFBQTtFQUNBLG1CQUFBO0FDSko7QURPQTtFQUNJLCtCQUFBO0VBQ0Esb0JBQUE7QUNMSjtBQ0xBO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBRE9KO0FDTkk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QURRUjtBQ1BRO0VBQ0ksb0JBQUE7RUFDQSxtQkFBQTtBRFNaO0FDUFE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0NBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxnQ0FBQTtBRFNaO0FDUlk7RUFDSSxhQUFBO0VBQ0EsZ0NBQUE7RUFDQSxjQUFBO0FEVWhCO0FDUFE7RUFDSSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QURTWjtBQ1JZO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0FEVWhCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb2xsZWN0aW9uL2NvbGxlY3Rpb24tbWFpbi9jb2xsZWN0aW9uLW1haW4uY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY29sb3ItYmFja2dyb3VuZDogIzgzNTM3RjtcbkBjb2xvci1iYWNrZ3JvdW5kLXNlY29uZGFyeTogI2YzZjNmZjtcbkBjb2xvci1hY2NlbnQ6ICNEQTYyN0Q7XG5AcGFnZS13aWR0aDogMTAyNHB4O1xuXG5AZm9udC1mYWNlIHtcbiAgICBzcmM6IHVybChcIkluZGllRmxvd2VyLnR0ZlwiKTtcbiAgICBmb250LWZhbWlseTogZmxvd2VyO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgICBzcmM6IHVybChcIlJhbGV3YXktUmVndWxhci50dGZcIik7XG4gICAgZm9udC1mYW1pbHk6IHJhbGV3YXk7XG59IiwiQGZvbnQtZmFjZSB7XG4gIHNyYzogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL0luZGllRmxvd2VyLnR0ZlwiKTtcbiAgZm9udC1mYW1pbHk6IGZsb3dlcjtcbn1cbkBmb250LWZhY2Uge1xuICBzcmM6IHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9SYWxld2F5LVJlZ3VsYXIudHRmXCIpO1xuICBmb250LWZhbWlseTogcmFsZXdheTtcbn1cbmZvcm0ge1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICM4MzUzN0Y7XG4gIGNvbG9yOiAjZmFmYWZhO1xuICBwYWRkaW5nOiA0ZW07XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuZm9ybSA+IGRpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nOiAwLjVlbTtcbn1cbmZvcm0gPiBkaXYgPiBsYWJlbCB7XG4gIHBhZGRpbmctcmlnaHQ6IDAuNWVtO1xuICBtYXJnaW4tcmlnaHQ6IDAuNWVtO1xufVxuZm9ybSA+IGRpdiA+IGlucHV0IHtcbiAgcGFkZGluZzogMC41ZW0gMDtcbiAgbWFyZ2luOiAtMC41ZW07XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkYmRiZGI7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGNvbG9yOiAjZmFmYWZhO1xuICB3aWR0aDogMjIwcHg7XG4gIHRyYW5zaXRpb246IDAuMXMgZWFzZS1pbi1vdXQgYWxsO1xufVxuZm9ybSA+IGRpdiA+IGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNlNjk0YTY7XG4gIGNvbG9yOiAjZTY5NGE2O1xufVxuZm9ybSA+IGRpdiA+IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNEQTYyN0Q7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiAwLjVlbTtcbiAgYm9yZGVyOiBub25lO1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICBjb2xvcjogI2ZhZmFmYTtcbiAgbWFyZ2luLXRvcDogMmVtO1xuICBtYXJnaW4tYm90dG9tOiAtMmVtO1xufVxuZm9ybSA+IGRpdiA+IGJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjk0YTY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbiIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uL2Fzc2V0cy92YXJpYWJsZXMubGVzcyc7XG5cbmZvcm0ge1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IEBjb2xvci1iYWNrZ3JvdW5kO1xuICAgIGNvbG9yOiAjZmFmYWZhO1xuICAgIHBhZGRpbmc6IDRlbTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAmID4gZGl2IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IC41ZW07XG4gICAgICAgICYgPiBsYWJlbCB7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAuNWVtO1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAuNWVtO1xuICAgICAgICB9XG4gICAgICAgICYgPiBpbnB1dCB7XG4gICAgICAgICAgICBwYWRkaW5nOiAuNWVtIDA7XG4gICAgICAgICAgICBtYXJnaW46IC0uNWVtO1xuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIGRhcmtlbigjZmFmYWZhLCAxMik7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgICAgIGNvbG9yOiAjZmFmYWZhO1xuICAgICAgICAgICAgd2lkdGg6IDIyMHB4O1xuICAgICAgICAgICAgdHJhbnNpdGlvbjogLjFzIGVhc2UtaW4tb3V0IGFsbDtcbiAgICAgICAgICAgICY6Zm9jdXMge1xuICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIGxpZ2h0ZW4oQGNvbG9yLWFjY2VudCwgMTIpO1xuICAgICAgICAgICAgICAgIGNvbG9yOiBsaWdodGVuKEBjb2xvci1hY2NlbnQsIDEyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmID4gYnV0dG9uIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IEBjb2xvci1hY2NlbnQ7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIHBhZGRpbmc6IC41ZW07XG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDEyMHB4O1xuICAgICAgICAgICAgY29sb3I6ICNmYWZhZmE7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyZW07XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAtMmVtO1xuICAgICAgICAgICAgJjpob3ZlciB7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRlbihAY29sb3ItYWNjZW50LCAxMik7XG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9XG5cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/collection/collection-main/collection-main.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/collection/collection-main/collection-main.component.ts ***!
  \************************************************************************************/
/*! exports provided: CollectionMainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionMainComponent", function() { return CollectionMainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_collection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/collection.service */ "./src/app/services/collection.service.ts");



var CollectionMainComponent = /** @class */ (function () {
    function CollectionMainComponent(collectionService) {
        this.collectionService = collectionService;
    }
    CollectionMainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.collectionService.list();
        this.collectionService.collection.subscribe(function (r) { return _this.collections = r; });
    };
    CollectionMainComponent.prototype.create = function (input) {
        var _this = this;
        var collection = {
            collectionName: input.value,
            id: '00000000-0000-0000-0000-000000000000'
        };
        input.value = '';
        this.collectionService.post(collection).subscribe(function (r) { return _this.collectionService.list(); });
    };
    CollectionMainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-collection-main',
            template: __webpack_require__(/*! ./collection-main.component.html */ "./src/app/components/collection/collection-main/collection-main.component.html"),
            styles: [__webpack_require__(/*! ./collection-main.component.less */ "./src/app/components/collection/collection-main/collection-main.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_collection_service__WEBPACK_IMPORTED_MODULE_2__["CollectionService"]])
    ], CollectionMainComponent);
    return CollectionMainComponent;
}());



/***/ }),

/***/ "./src/app/components/home/card-browser/card-browser.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/components/home/card-browser/card-browser.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  card-browser works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/home/card-browser/card-browser.component.less":
/*!**************************************************************************!*\
  !*** ./src/app/components/home/card-browser/card-browser.component.less ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9jYXJkLWJyb3dzZXIvY2FyZC1icm93c2VyLmNvbXBvbmVudC5sZXNzIn0= */"

/***/ }),

/***/ "./src/app/components/home/card-browser/card-browser.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/home/card-browser/card-browser.component.ts ***!
  \************************************************************************/
/*! exports provided: CardBrowserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardBrowserComponent", function() { return CardBrowserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CardBrowserComponent = /** @class */ (function () {
    function CardBrowserComponent() {
    }
    CardBrowserComponent.prototype.ngOnInit = function () {
    };
    CardBrowserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-card-browser',
            template: __webpack_require__(/*! ./card-browser.component.html */ "./src/app/components/home/card-browser/card-browser.component.html"),
            styles: [__webpack_require__(/*! ./card-browser.component.less */ "./src/app/components/home/card-browser/card-browser.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CardBrowserComponent);
    return CardBrowserComponent;
}());



/***/ }),

/***/ "./src/app/components/home/collection-card/collection-card.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/components/home/collection-card/collection-card.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  collection-card works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/home/collection-card/collection-card.component.less":
/*!********************************************************************************!*\
  !*** ./src/app/components/home/collection-card/collection-card.component.less ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9jb2xsZWN0aW9uLWNhcmQvY29sbGVjdGlvbi1jYXJkLmNvbXBvbmVudC5sZXNzIn0= */"

/***/ }),

/***/ "./src/app/components/home/collection-card/collection-card.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/components/home/collection-card/collection-card.component.ts ***!
  \******************************************************************************/
/*! exports provided: CollectionCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionCardComponent", function() { return CollectionCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CollectionCardComponent = /** @class */ (function () {
    function CollectionCardComponent() {
    }
    CollectionCardComponent.prototype.ngOnInit = function () {
    };
    CollectionCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-collection-card',
            template: __webpack_require__(/*! ./collection-card.component.html */ "./src/app/components/home/collection-card/collection-card.component.html"),
            styles: [__webpack_require__(/*! ./collection-card.component.less */ "./src/app/components/home/collection-card/collection-card.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CollectionCardComponent);
    return CollectionCardComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Velkommen til Feriephoto</h1>\n\n<div class=\"instruction-box\">\n  <div>\n    <h2>1. Opret</h2>\n    <p>Opret en ny kollektion for at komme igang</p>\n  </div>\n  <div>\n    <h2>2. Genemse</h2>\n    <p>Gennemse billederne, og tilføj de bedste til din kollektion</p>\n  </div>\n  <div>\n    <h2>3. Download</h2>\n    <p>Gem billederne på din egen pc, ved at downloade kollektionen</p>\n  </div>\n</div>\n<h1 class=\"mt-2\">Kollektioner</h1>\n<app-collection-card></app-collection-card>"

/***/ }),

/***/ "./src/app/components/home/home.component.less":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.less ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  src: url('IndieFlower.ttf');\n  font-family: flower;\n}\n@font-face {\n  src: url('Raleway-Regular.ttf');\n  font-family: raleway;\n}\nul {\n  padding-left: 1em;\n}\nol {\n  padding-left: 1em;\n}\n.instruction-box {\n  width: 100%;\n  height: 220px;\n  background-color: #935d8e;\n  display: flex;\n  border-radius: 10px;\n}\n.instruction-box > div {\n  flex-grow: 1;\n  max-width: 33.33%;\n  padding: 1em;\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n  flex-direction: column;\n  transition: 0.1s ease-in-out all;\n}\n.instruction-box > div:nth-child(2n) {\n  background-color: #a672a2;\n}\n.instruction-box > div:hover {\n  background-color: #b488b0;\n}\n.instruction-box > div > h2 {\n  color: #fafafa;\n  align-self: center;\n}\n.instruction-box > div > p {\n  font-weight: bold;\n  color: #fefefe;\n  margin-top: 2em;\n  text-align: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL0U6L0RldmVsb3AvSW1hZ2VQaWNrZXIvZGV2LmNocmlzdGVuc2VuLmltYWdlcGlja2VyLndlYi9zcmMvYXNzZXRzL3ZhcmlhYmxlcy5sZXNzIiwic3JjL2FwcC9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9ob21lL0U6L0RldmVsb3AvSW1hZ2VQaWNrZXIvZGV2LmNocmlzdGVuc2VuLmltYWdlcGlja2VyLndlYi9zcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5sZXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtBQ0pKO0FET0E7RUFDSSwrQkFBQTtFQUNBLG9CQUFBO0FDTEo7QUNMQTtFQUNJLGlCQUFBO0FET0o7QUNKQTtFQUNJLGlCQUFBO0FETUo7QUNIQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QURLSjtBQ0pJO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQ0FBQTtBRE1SO0FDTFE7RUFDSSx5QkFBQTtBRE9aO0FDSlE7RUFDSSx5QkFBQTtBRE1aO0FDSFE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QURLWjtBQ0hRO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FES1oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQubGVzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjb2xvci1iYWNrZ3JvdW5kOiAjODM1MzdGO1xuQGNvbG9yLWJhY2tncm91bmQtc2Vjb25kYXJ5OiAjZjNmM2ZmO1xuQGNvbG9yLWFjY2VudDogI0RBNjI3RDtcbkBwYWdlLXdpZHRoOiAxMDI0cHg7XG5cbkBmb250LWZhY2Uge1xuICAgIHNyYzogdXJsKFwiSW5kaWVGbG93ZXIudHRmXCIpO1xuICAgIGZvbnQtZmFtaWx5OiBmbG93ZXI7XG59XG5cbkBmb250LWZhY2Uge1xuICAgIHNyYzogdXJsKFwiUmFsZXdheS1SZWd1bGFyLnR0ZlwiKTtcbiAgICBmb250LWZhbWlseTogcmFsZXdheTtcbn0iLCJAZm9udC1mYWNlIHtcbiAgc3JjOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvSW5kaWVGbG93ZXIudHRmXCIpO1xuICBmb250LWZhbWlseTogZmxvd2VyO1xufVxuQGZvbnQtZmFjZSB7XG4gIHNyYzogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL1JhbGV3YXktUmVndWxhci50dGZcIik7XG4gIGZvbnQtZmFtaWx5OiByYWxld2F5O1xufVxudWwge1xuICBwYWRkaW5nLWxlZnQ6IDFlbTtcbn1cbm9sIHtcbiAgcGFkZGluZy1sZWZ0OiAxZW07XG59XG4uaW5zdHJ1Y3Rpb24tYm94IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5MzVkOGU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uaW5zdHJ1Y3Rpb24tYm94ID4gZGl2IHtcbiAgZmxleC1ncm93OiAxO1xuICBtYXgtd2lkdGg6IDMzLjMzJTtcbiAgcGFkZGluZzogMWVtO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHRyYW5zaXRpb246IDAuMXMgZWFzZS1pbi1vdXQgYWxsO1xufVxuLmluc3RydWN0aW9uLWJveCA+IGRpdjpudGgtY2hpbGQoMm4pIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2E2NzJhMjtcbn1cbi5pbnN0cnVjdGlvbi1ib3ggPiBkaXY6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjQ4OGIwO1xufVxuLmluc3RydWN0aW9uLWJveCA+IGRpdiA+IGgyIHtcbiAgY29sb3I6ICNmYWZhZmE7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cbi5pbnN0cnVjdGlvbi1ib3ggPiBkaXYgPiBwIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjZmVmZWZlO1xuICBtYXJnaW4tdG9wOiAyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbiIsIkBpbXBvcnQgJy4uLy4uLy4uL2Fzc2V0cy92YXJpYWJsZXMubGVzcyc7XG5cbnVsIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDFlbTtcbn1cblxub2wge1xuICAgIHBhZGRpbmctbGVmdDogMWVtO1xufVxuXG4uaW5zdHJ1Y3Rpb24tYm94IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDIyMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0ZW4oQGNvbG9yLWJhY2tncm91bmQsIDUpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAmID4gZGl2IHtcbiAgICAgICAgZmxleC1ncm93OiAxO1xuICAgICAgICBtYXgtd2lkdGg6IDMzLjMzJTtcbiAgICAgICAgcGFkZGluZzogMWVtO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIHRyYW5zaXRpb246IC4xcyBlYXNlLWluLW91dCBhbGw7XG4gICAgICAgICY6bnRoLWNoaWxkKDJuKSB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGVuKEBjb2xvci1iYWNrZ3JvdW5kLCAxMyk7XG4gICAgICAgIH1cblxuICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0ZW4oQGNvbG9yLWJhY2tncm91bmQsIDIwKTtcbiAgICAgICAgfVxuXG4gICAgICAgICYgPiBoMiB7XG4gICAgICAgICAgICBjb2xvcjogI2ZhZmFmYTtcbiAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgfVxuICAgICAgICAmID4gcCB7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGNvbG9yOiAjZmVmZWZlO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMmVtO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB9XG4gICAgfVxuICAgIFxufVxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent(authService) {
        this.authService = authService;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.less */ "./src/app/components/home/home.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form>\n  <div>\n      <label for=\"username\">Brugernavn:</label>\n      <input type=\"text\" id=\"username\" #usr>\n  </div>\n  <div>\n      <label for=\"pw\">Kode:</label>\n      <input type=\"password\" id=\"pw\" #pw>\n  </div>\n  <div>\n    <button (click)=\"signin(usr.value, pw.value)\">Login</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/components/login/login.component.less":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.less ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  src: url('IndieFlower.ttf');\n  font-family: flower;\n}\n@font-face {\n  src: url('Raleway-Regular.ttf');\n  font-family: raleway;\n}\nform {\n  max-width: 600px;\n  margin: 0 auto;\n  width: 100%;\n  background-color: #e6e6e6;\n  padding: 4em;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\nform > div {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 0.5em;\n}\nform > div > label {\n  width: 120px;\n}\nform > div > input {\n  padding: 0.25em;\n  border: 2px solid #dbdbdb;\n}\nform > div > button {\n  background-color: #DA627D;\n  font-weight: bold;\n  padding: 0.5em;\n  border: none;\n  min-width: 120px;\n  color: #fafafa;\n  margin-top: 2em;\n  margin-bottom: -2em;\n}\nform > div > button:hover {\n  background-color: #e694a6;\n  cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9FOi9EZXZlbG9wL0ltYWdlUGlja2VyL2Rldi5jaHJpc3RlbnNlbi5pbWFnZXBpY2tlci53ZWIvc3JjL2Fzc2V0cy92YXJpYWJsZXMubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9FOi9EZXZlbG9wL0ltYWdlUGlja2VyL2Rldi5jaHJpc3RlbnNlbi5pbWFnZXBpY2tlci53ZWIvc3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5sZXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtBQ0pKO0FET0E7RUFDSSwrQkFBQTtFQUNBLG9CQUFBO0FDTEo7QUNMQTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FET0o7QUNOSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBRFFSO0FDUFE7RUFDSSxZQUFBO0FEU1o7QUNQUTtFQUNJLGVBQUE7RUFDQSx5QkFBQTtBRFNaO0FDUFE7RUFDSSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QURTWjtBQ1JZO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0FEVWhCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQubGVzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjb2xvci1iYWNrZ3JvdW5kOiAjODM1MzdGO1xuQGNvbG9yLWJhY2tncm91bmQtc2Vjb25kYXJ5OiAjZjNmM2ZmO1xuQGNvbG9yLWFjY2VudDogI0RBNjI3RDtcbkBwYWdlLXdpZHRoOiAxMDI0cHg7XG5cbkBmb250LWZhY2Uge1xuICAgIHNyYzogdXJsKFwiSW5kaWVGbG93ZXIudHRmXCIpO1xuICAgIGZvbnQtZmFtaWx5OiBmbG93ZXI7XG59XG5cbkBmb250LWZhY2Uge1xuICAgIHNyYzogdXJsKFwiUmFsZXdheS1SZWd1bGFyLnR0ZlwiKTtcbiAgICBmb250LWZhbWlseTogcmFsZXdheTtcbn0iLCJAZm9udC1mYWNlIHtcbiAgc3JjOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvSW5kaWVGbG93ZXIudHRmXCIpO1xuICBmb250LWZhbWlseTogZmxvd2VyO1xufVxuQGZvbnQtZmFjZSB7XG4gIHNyYzogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL1JhbGV3YXktUmVndWxhci50dGZcIik7XG4gIGZvbnQtZmFtaWx5OiByYWxld2F5O1xufVxuZm9ybSB7XG4gIG1heC13aWR0aDogNjAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcbiAgcGFkZGluZzogNGVtO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbmZvcm0gPiBkaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMC41ZW07XG59XG5mb3JtID4gZGl2ID4gbGFiZWwge1xuICB3aWR0aDogMTIwcHg7XG59XG5mb3JtID4gZGl2ID4gaW5wdXQge1xuICBwYWRkaW5nOiAwLjI1ZW07XG4gIGJvcmRlcjogMnB4IHNvbGlkICNkYmRiZGI7XG59XG5mb3JtID4gZGl2ID4gYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RBNjI3RDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICBib3JkZXI6IG5vbmU7XG4gIG1pbi13aWR0aDogMTIwcHg7XG4gIGNvbG9yOiAjZmFmYWZhO1xuICBtYXJnaW4tdG9wOiAyZW07XG4gIG1hcmdpbi1ib3R0b206IC0yZW07XG59XG5mb3JtID4gZGl2ID4gYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2OTRhNjtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuIiwiQGltcG9ydCAnLi4vLi4vLi4vYXNzZXRzL3ZhcmlhYmxlcy5sZXNzJztcblxuZm9ybSB7XG4gICAgbWF4LXdpZHRoOiA2MDBweDtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBkYXJrZW4oI2ZhZmFmYSwgOCk7XG4gICAgcGFkZGluZzogNGVtO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICYgPiBkaXYge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZzogLjVlbTtcbiAgICAgICAgJiA+IGxhYmVsIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMjBweDtcbiAgICAgICAgfVxuICAgICAgICAmID4gaW5wdXQge1xuICAgICAgICAgICAgcGFkZGluZzogLjI1ZW07XG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCBkYXJrZW4oI2ZhZmFmYSwgMTIpO1xuICAgICAgICB9XG4gICAgICAgICYgPiBidXR0b24ge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogQGNvbG9yLWFjY2VudDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgcGFkZGluZzogLjVlbTtcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgIG1pbi13aWR0aDogMTIwcHg7XG4gICAgICAgICAgICBjb2xvcjogI2ZhZmFmYTtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDJlbTtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IC0yZW07XG4gICAgICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGVuKEBjb2xvci1hY2NlbnQsIDEyKTtcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cblxufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.signin = function (username, password) {
        var _this = this;
        this.auth.login(username, password).subscribe(function (r) {
            if (r) {
                _this.auth.user.username = r.username;
                _this.auth.isLoggedIn = true;
            }
            if (_this.auth.isLoggedIn) {
                _this.router.navigate(['']);
            }
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.less */ "./src/app/components/login/login.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/navigation/navigation.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"nav\">\n  <div class=\"content\">\n    <a href=\"javascript:void(0)\" [routerLink]='[\"\"]'><img src=\"../../../assets/feriephoto.png\" class=\"brand\"></a>\n    <ul>\n      <li><a href=\"javascript:void(0)\" [routerLink]='[\"collection\"]'>Kollektioner</a></li>\n      <li><a href=\"javascript:void(0)\" [routerLink]='[\"browse\"]'>Gennemse Billeder</a></li>\n      <li><a href=\"javascript:void(0)\" [routerLink]='[\"upload\"]'>Upload</a></li>\n    </ul>\n\n    <div class=\"badge\" *ngIf=\"auth.isLoggedIn\">\n      Velkommen {{auth.user.username}}\n    </div>\n  </div>\n\n\n</nav>"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.less":
/*!*****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.less ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  src: url('IndieFlower.ttf');\n  font-family: flower;\n}\n@font-face {\n  src: url('Raleway-Regular.ttf');\n  font-family: raleway;\n}\n.nav {\n  height: 85px;\n  width: 100%;\n  background-color: #f3f3ff;\n}\n.nav > .content {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  max-width: 1024px;\n  width: 100%;\n  height: 100%;\n  margin: 0 auto;\n  position: relative;\n}\n.brand {\n  height: 75px;\n  padding: 0.5em;\n}\nul {\n  list-style-type: none;\n  align-self: flex-end;\n  margin-right: -0.5em;\n}\nli {\n  display: inline-block;\n  padding: 0.5em 0.5em;\n}\nli > a {\n  text-decoration: none;\n  color: #DA627D;\n  font-weight: bold;\n  transition: 0.1s ease-in all;\n}\nli > a:hover {\n  border-bottom: 0.25em solid #DA627D;\n  color: #edb5c1;\n}\n.badge {\n  position: absolute;\n  top: 0.5em;\n  right: 0.5em;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXZpZ2F0aW9uL0U6L0RldmVsb3AvSW1hZ2VQaWNrZXIvZGV2LmNocmlzdGVuc2VuLmltYWdlcGlja2VyLndlYi9zcmMvYXNzZXRzL3ZhcmlhYmxlcy5sZXNzIiwic3JjL2FwcC9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9uYXZpZ2F0aW9uL0U6L0RldmVsb3AvSW1hZ2VQaWNrZXIvZGV2LmNocmlzdGVuc2VuLmltYWdlcGlja2VyLndlYi9zcmMvYXBwL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5sZXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtBQ0pKO0FET0E7RUFDSSwrQkFBQTtFQUNBLG9CQUFBO0FDTEo7QUNMQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7QURPSjtBQ05JO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FEUVI7QUNKQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0FETUo7QUNIQTtFQUNJLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxvQkFBQTtBREtKO0FDRkE7RUFDSSxxQkFBQTtFQUNBLG9CQUFBO0FESUo7QUNEQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7QURHSjtBQ0ZJO0VBQ0ksbUNBQUE7RUFDQSxjQUFBO0FESVI7QUNDQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7QURDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5sZXNzIiwic291cmNlc0NvbnRlbnQiOlsiQGNvbG9yLWJhY2tncm91bmQ6ICM4MzUzN0Y7XG5AY29sb3ItYmFja2dyb3VuZC1zZWNvbmRhcnk6ICNmM2YzZmY7XG5AY29sb3ItYWNjZW50OiAjREE2MjdEO1xuQHBhZ2Utd2lkdGg6IDEwMjRweDtcblxuQGZvbnQtZmFjZSB7XG4gICAgc3JjOiB1cmwoXCJJbmRpZUZsb3dlci50dGZcIik7XG4gICAgZm9udC1mYW1pbHk6IGZsb3dlcjtcbn1cblxuQGZvbnQtZmFjZSB7XG4gICAgc3JjOiB1cmwoXCJSYWxld2F5LVJlZ3VsYXIudHRmXCIpO1xuICAgIGZvbnQtZmFtaWx5OiByYWxld2F5O1xufSIsIkBmb250LWZhY2Uge1xuICBzcmM6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9JbmRpZUZsb3dlci50dGZcIik7XG4gIGZvbnQtZmFtaWx5OiBmbG93ZXI7XG59XG5AZm9udC1mYWNlIHtcbiAgc3JjOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvUmFsZXdheS1SZWd1bGFyLnR0ZlwiKTtcbiAgZm9udC1mYW1pbHk6IHJhbGV3YXk7XG59XG4ubmF2IHtcbiAgaGVpZ2h0OiA4NXB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzZjNmZjtcbn1cbi5uYXYgPiAuY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWF4LXdpZHRoOiAxMDI0cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uYnJhbmQge1xuICBoZWlnaHQ6IDc1cHg7XG4gIHBhZGRpbmc6IDAuNWVtO1xufVxudWwge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xuICBtYXJnaW4tcmlnaHQ6IC0wLjVlbTtcbn1cbmxpIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiAwLjVlbSAwLjVlbTtcbn1cbmxpID4gYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6ICNEQTYyN0Q7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0cmFuc2l0aW9uOiAwLjFzIGVhc2UtaW4gYWxsO1xufVxubGkgPiBhOmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMC4yNWVtIHNvbGlkICNEQTYyN0Q7XG4gIGNvbG9yOiAjZWRiNWMxO1xufVxuLmJhZGdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDAuNWVtO1xuICByaWdodDogMC41ZW07XG59XG4iLCJAaW1wb3J0ICcuLi8uLi8uLi9hc3NldHMvdmFyaWFibGVzLmxlc3MnO1xuXG4ubmF2IHtcbiAgICBoZWlnaHQ6IDg1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogQGNvbG9yLWJhY2tncm91bmQtc2Vjb25kYXJ5O1xuICAgICYgPiAuY29udGVudCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgbWF4LXdpZHRoOiBAcGFnZS13aWR0aDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB9XG59XG5cbi5icmFuZCB7XG4gICAgaGVpZ2h0OiA3NXB4O1xuICAgIHBhZGRpbmc6IC41ZW07XG59XG5cbnVsIHtcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAtLjVlbTtcbn1cblxubGkge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nOiAuNWVtIC41ZW07XG59XG5cbmxpID4gYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiBAY29sb3ItYWNjZW50O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRyYW5zaXRpb246IC4xcyBlYXNlLWluIGFsbDtcbiAgICAmOmhvdmVyIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogLjI1ZW0gc29saWQgQGNvbG9yLWFjY2VudDtcbiAgICAgICAgY29sb3I6IGxpZ2h0ZW4oQGNvbG9yLWFjY2VudCwgMjApO1xuICAgIH1cblxufVxuXG4uYmFkZ2Uge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC41ZW07XG4gICAgcmlnaHQ6IC41ZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.ts ***!
  \***************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");



var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(auth) {
        this.auth = auth;
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/components/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.less */ "./src/app/components/navigation/navigation.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/components/picture/browser/browser.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/picture/browser/browser.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"image-viewer\">\n  <div class=\"collection-selector\">\n    <select [ngModel]=\"activeCollection.id\">\n      <option *ngFor=\"let option of collections\" [value]=\"option.id\">{{option.collectionName}}</option>\n    </select>\n  </div>\n  <div class=\"image-container\">\n    <span>Forrige</span>\n    <img [src]=\"imgSrc\">\n    <span>Næste</span>\n  </div>\n  <div class=\"img-controls\">\n    <button>Tilføj Til Kollektion</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/picture/browser/browser.component.less":
/*!*******************************************************************!*\
  !*** ./src/app/components/picture/browser/browser.component.less ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGljdHVyZS9icm93c2VyL2Jyb3dzZXIuY29tcG9uZW50Lmxlc3MifQ== */"

/***/ }),

/***/ "./src/app/components/picture/browser/browser.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/picture/browser/browser.component.ts ***!
  \*****************************************************************/
/*! exports provided: BrowserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserComponent", function() { return BrowserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_model_collection_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/model/collection.model */ "./src/app/model/collection.model.ts");
/* harmony import */ var src_app_services_collection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/collection.service */ "./src/app/services/collection.service.ts");




var BrowserComponent = /** @class */ (function () {
    function BrowserComponent(collectionService) {
        this.collectionService = collectionService;
        this.activeCollection = new src_app_model_collection_model__WEBPACK_IMPORTED_MODULE_2__["Collection"]();
    }
    BrowserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.collectionService.collection.subscribe(function (r) {
            _this.collections = r;
            _this.activeCollection = _this.collections[0];
        });
        this.collectionService.list();
    };
    BrowserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-browser',
            template: __webpack_require__(/*! ./browser.component.html */ "./src/app/components/picture/browser/browser.component.html"),
            styles: [__webpack_require__(/*! ./browser.component.less */ "./src/app/components/picture/browser/browser.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_collection_service__WEBPACK_IMPORTED_MODULE_3__["CollectionService"]])
    ], BrowserComponent);
    return BrowserComponent;
}());



/***/ }),

/***/ "./src/app/components/upload/upload.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/upload/upload.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form>\n  <h1>Upload file</h1>\n  <input #file type=\"file\" multiple (change)=\"selectFiles(file.files)\">\n</form>\n<button (click)=\"upload()\">Click me</button>\n<br>"

/***/ }),

/***/ "./src/app/components/upload/upload.component.less":
/*!*********************************************************!*\
  !*** ./src/app/components/upload/upload.component.less ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQubGVzcyJ9 */"

/***/ }),

/***/ "./src/app/components/upload/upload.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/upload/upload.component.ts ***!
  \*******************************************************/
/*! exports provided: UploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _image_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../image.service */ "./src/app/image.service.ts");



var UploadComponent = /** @class */ (function () {
    function UploadComponent(imageService) {
        this.imageService = imageService;
        this.files = [];
    }
    UploadComponent.prototype.ngOnInit = function () {
    };
    UploadComponent.prototype.selectFiles = function (files) {
        if (files.length === 0) {
            return;
        }
        this.files = files;
    };
    UploadComponent.prototype.upload = function () {
        if (this.files.length === 0) {
            return;
        }
        var formData = new FormData();
        for (var _i = 0, _a = this.files; _i < _a.length; _i++) {
            var file = _a[_i];
            formData.append(file.name, file);
        }
        this.imageService.upload(formData).subscribe(function (r) { return console.log(r); });
    };
    UploadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-upload',
            template: __webpack_require__(/*! ./upload.component.html */ "./src/app/components/upload/upload.component.html"),
            styles: [__webpack_require__(/*! ./upload.component.less */ "./src/app/components/upload/upload.component.less")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_image_service__WEBPACK_IMPORTED_MODULE_2__["ImageService"]])
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/image.service.ts":
/*!**********************************!*\
  !*** ./src/app/image.service.ts ***!
  \**********************************/
/*! exports provided: ImageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageService", function() { return ImageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var ImageService = /** @class */ (function () {
    function ImageService(http) {
        this.http = http;
    }
    ImageService.prototype.getAThing = function () {
        return this.http.get('https://localhost:44341/api/image/all');
    };
    ImageService.prototype.upload = function (formData) {
        var request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpRequest"]('POST', 'https://localhost:44341/api/image/upload', formData);
        return this.http.request(request);
    };
    ImageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ImageService);
    return ImageService;
}());



/***/ }),

/***/ "./src/app/model/collection.model.ts":
/*!*******************************************!*\
  !*** ./src/app/model/collection.model.ts ***!
  \*******************************************/
/*! exports provided: Collection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Collection", function() { return Collection; });
var Collection = /** @class */ (function () {
    function Collection() {
    }
    return Collection;
}());



/***/ }),

/***/ "./src/app/model/user.model.ts":
/*!*************************************!*\
  !*** ./src/app/model/user.model.ts ***!
  \*************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/services/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.collection = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.baseUrl = "" + src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    ApiService.prototype.setEndpoint = function (endpoint) {
        this.endpoint = this.baseUrl + endpoint;
    };
    ApiService.prototype.get = function (id) {
        return this.http.get(this.endpoint + '/' + id);
    };
    ApiService.prototype.list = function () {
        var _this = this;
        this.http.get(this.endpoint).subscribe(function (r) { return _this.collection.emit(r); });
    };
    ApiService.prototype.post = function (t) {
        return this.http.post(this.endpoint, t);
    };
    ApiService.prototype.delete = function (id) {
        return this.http.delete(this.endpoint + '/' + id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ApiService.prototype, "collection", void 0);
    ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/user.model */ "./src/app/model/user.model.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");





var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.user = new _model_user_model__WEBPACK_IMPORTED_MODULE_2__["User"]();
        this.endpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "auth/login";
        this.isLoggedIn = false;
    }
    AuthService.prototype.login = function (username, password) {
        return this.http.get(this.endpoint + "?username=" + username + "&password=" + password);
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/collection.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/collection.service.ts ***!
  \************************************************/
/*! exports provided: CollectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionService", function() { return CollectionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var CollectionService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CollectionService, _super);
    function CollectionService(http) {
        var _this = _super.call(this, http) || this;
        _this.setEndpoint('collection');
        return _this;
    }
    CollectionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], CollectionService);
    return CollectionService;
}(_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]));



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: 'https://localhost:44341/api/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Develop\ImagePicker\dev.christensen.imagepicker.web\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map