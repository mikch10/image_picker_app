import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Collection } from '../model/collection.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService<T> {
  @Output() collection: EventEmitter<T[]> = new EventEmitter();
  public baseUrl: string;
  public endpoint: string;
  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.apiUrl}`;
  }

  setEndpoint(endpoint: string) {
    this.endpoint = this.baseUrl + endpoint;
  }

  get(id: string): Observable<T> {
    return this.http.get<T>(this.endpoint + '/' + id);
  }

  list() {
    this.http.get<T[]>(this.endpoint).subscribe(r => this.collection.emit(r));
  }

  post(t: T): Observable<T> {
    return this.http.post<T>(this.endpoint, t);
  }

  delete(id: string): Observable<HttpResponse<number>> {
    return this.http.delete<HttpResponse<number>>(this.endpoint + '/' + id);
  }

  HTTP(): HttpClient {
    return this.http;
  }

}
