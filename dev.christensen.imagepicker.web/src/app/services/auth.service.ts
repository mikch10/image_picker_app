import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean;
  redirectUrl: string;

  user: User;
  endpoint: string;
  constructor(private http: HttpClient) {
    this.user = new User();
    this.endpoint = `${environment.apiUrl}auth/login`;
    this.isLoggedIn = false;
  }

  login(username: string, password: string): Observable<User> {
    return this.http.get<User>(`${this.endpoint}?username=${username}&password=${password}`);
  }


}
