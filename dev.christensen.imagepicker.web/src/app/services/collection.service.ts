import { Injectable, Output } from '@angular/core';
import { ApiService } from './api.service';
import { Collection } from '../model/collection.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CollectionService extends ApiService<Collection> {

  constructor(http: HttpClient) {
    super(http);
    this.setEndpoint('collection');
  }
}
