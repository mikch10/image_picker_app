import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Image } from '../model/image.model';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService extends ApiService<Image> {

  constructor(http: HttpClient) {
    super(http);
    this.setEndpoint('image');
  }

  upload(formData: FormData): any {
    const request = new HttpRequest('POST', 'https://localhost:44341/api/image/upload', formData);
    return this.HTTP().request(request);
  }

  count(): Observable<number> {
    return this.HTTP().get<number>(this.endpoint + '/count');
  }

}
