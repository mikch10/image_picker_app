import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './components/upload/upload.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './components/login/login.component';
import { CollectionMainComponent } from './components/collection/collection-main/collection-main.component';
import { BrowserComponent } from './components/picture/browser/browser.component';


const routes: Routes = [
  {path: '', component: HomeComponent,  canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'collection', component: CollectionMainComponent, canActivate: [AuthGuard]},
  {path: 'browse', component: BrowserComponent, canActivate: [AuthGuard]},
  {path: 'upload', component: UploadComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
