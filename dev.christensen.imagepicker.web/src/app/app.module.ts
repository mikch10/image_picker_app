import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UploadComponent } from './components/upload/upload.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LoginComponent } from './components/login/login.component';
import { CollectionMainComponent } from './components/collection/collection-main/collection-main.component';
import { BrowserComponent } from './components/picture/browser/browser.component';
import { CollectionCardComponent } from './components/home/collection-card/collection-card.component';
import { CardBrowserComponent } from './components/home/card-browser/card-browser.component';
import { CollectionItemComponent } from './components/collection/collection-item/collection-item.component';
import { CollectionListComponent } from './components/collection/collection-list/collection-list.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    HomeComponent,
    NavigationComponent,
    LoginComponent,
    CollectionMainComponent,
    BrowserComponent,
    CollectionCardComponent,
    CardBrowserComponent,
    CollectionItemComponent,
    CollectionListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
