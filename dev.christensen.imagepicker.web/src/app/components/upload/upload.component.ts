import { Component, OnInit } from '@angular/core';
import { ImageService } from '../../services/image.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.less']
})
export class UploadComponent implements OnInit {
  files: File[];
  constructor(private imageService: ImageService) {
    this.files = [];
  }

  ngOnInit() {
  }

  selectFiles(files: File[]) {
    if (files.length === 0) {
      return;
    }
    this.files = [];
    for (const f of files) {
      this.files.push(f);
    }
  }

  upload() {
    if (this.files.length === 0) {
      return;
    }
    const formData = new FormData();
    const poppedFile = this.files.pop();
    formData.append(poppedFile.name.split('.')[0], poppedFile);
    this.imageService.upload(formData).subscribe(r => this.upload());

  }

}
