import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signin(username: string, password: string) {
    this.auth.login(username, password).subscribe(r => {
      if (r) {
        this.auth.user.username = r.username;
        this.auth.isLoggedIn = true;
      }
      if (this.auth.isLoggedIn) {
        this.router.navigate(['']);
      }
    });
  }

}
