import { Component, OnInit, Input } from '@angular/core';
import { Collection } from '../../../model/collection.model';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.less']
})
export class CollectionListComponent implements OnInit {
  @Input() collections: Collection[];
  constructor() { }

  ngOnInit() {
  }

}
