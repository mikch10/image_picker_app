import { Component, OnInit, Input } from '@angular/core';
import { Collection } from '../../../model/collection.model';
import { CollectionService } from 'src/app/services/collection.service';

@Component({
  selector: 'app-collection-item',
  templateUrl: './collection-item.component.html',
  styleUrls: ['./collection-item.component.less']
})
export class CollectionItemComponent implements OnInit {
  @Input() collection: Collection;
  constructor(private collectionService: CollectionService) { }

  ngOnInit() {
  }

  delete() {
    this.collectionService.delete(this.collection.id).subscribe(r => this.collectionService.list());
  }

}
