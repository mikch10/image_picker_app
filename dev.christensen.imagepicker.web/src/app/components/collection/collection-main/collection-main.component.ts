import { Component, OnInit } from '@angular/core';
import { CollectionService } from '../../../services/collection.service';
import { Collection } from '../../../model/collection.model';

@Component({
  selector: 'app-collection-main',
  templateUrl: './collection-main.component.html',
  styleUrls: ['./collection-main.component.less']
})
export class CollectionMainComponent implements OnInit {
  collections: Collection[];
  constructor(private collectionService: CollectionService) { }

  ngOnInit() {
    this.collectionService.list();
    this.collectionService.collection.subscribe(r => this.collections = r);
  }

  create(input: HTMLInputElement) {
    const collection: Collection =  {
      collectionName: input.value,
      id: '00000000-0000-0000-0000-000000000000'
    };
    input.value = '';
    this.collectionService.post(collection).subscribe(r => this.collectionService.list());
  }

}
