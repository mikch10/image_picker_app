import { Component, OnInit } from '@angular/core';
import { Collection } from '../../../model/collection.model';
import { CollectionService } from '../../../services/collection.service';
import { ImageService } from '../../../services/image.service';

@Component({
  selector: 'app-browser',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.less']
})
export class BrowserComponent implements OnInit {
  collections: Collection[];
  activeCollection: Collection;

  imageList: string[];
  imageIndex: number;

  constructor(private collectionService: CollectionService, public imageService: ImageService) {
    this.activeCollection = new Collection();
    this.imageList = [];
    this.imageIndex = 0;
  }

  ngOnInit() {
    this.collectionService.collection.subscribe(r => {
      this.collections = r;
      this.activeCollection = this.collections[0];
    });
    this.collectionService.list();

    this.imageService.collection.subscribe(r => {
      this.imageList = r;
    });
    this.imageService.list();
  }

  page(num: number) {
    this.imageIndex += num;
    if (this.imageIndex >= this.imageList.length) {
      this.imageIndex = 0;
    }
    if (this.imageIndex < 0) {
      this.imageIndex = this.imageList.length - 1;
    }
  }

}
