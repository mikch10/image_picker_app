﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dev.christensen.imagepicker.model
{
    public class Collection : Entity
    {
        public string CollectionName { get; set; }
        ICollection<ImageCollection> ImageCollection { get; set; }

    }
}
