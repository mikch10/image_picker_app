﻿using System;
using System.Collections.Generic;

namespace dev.christensen.imagepicker.model
{
    public class Image : Entity
    {
        public string ImageTitle { get; set; }
        public byte[] ImageData { get; set; }
        public ICollection<ImageCollection> ImageCollection { get; set; }

    }
}
