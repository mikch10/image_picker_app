﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dev.christensen.imagepicker.model
{
    public class ImageCollection : Entity
    {
        public Guid CollectionId { get; set; }
        [ForeignKey("CollectionId")]
        public Collection Collection { get; set; }
        public Guid ImageId { get; set; }
        [ForeignKey("ImageId")]
        public Image Image { get; set; }


    }
}
