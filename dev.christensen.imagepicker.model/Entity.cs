﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dev.christensen.imagepicker.model
{
    public class Entity
    {
        public Guid Id { get; set; }
        public Entity()
        {
            Id = Guid.NewGuid();
        }
    }
}
